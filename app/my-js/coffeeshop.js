'use strict';

var queryObject = queryString.getObject(window.location.search);


(function IIFE(coffee){
    var pageTitle = document.querySelector('title');
    pageTitle.innerHTML = coffee.venue.name;

    var descriptionElement = document.querySelector('.description');
    descriptionElement.innerHTML =
        `
        <div class="row justify-content-center">
            <div class="col-md-6 col-sm-8 col-10">
                <div class="main-info">
                    <div class="row">
                        <div class="col-md-9 col-sm-7 col-12">
                            <h2>${coffee.venue.name}</h2>
                        </div>
                        <div class="col-md-3 col-sm-5 col-12">
                            <p class="distance-icon"><i class="fa fa-ellipsis-h" aria-hidden="true"></i>
<i class="fa fa-map-marker" aria-hidden="true"></i></p>
                            <p class="distance-info">${coffee.venue.location.distance}m</p>
                        </div>
                    </div>             
                </div>
            </div>
        </div>
        
        
        <div class="row justify-content-center">
            <div class="col-md-6 col-sm-8 col-10">
                <p>price range: ${coffee.venue.price.tier} - ${coffee.venue.price.message}</p>
            </div>
        </div>`;

    var photosUrl = venueItemsQuery.url(coffee.venue.id, 'photos');
    var tipsUrl = venueItemsQuery.url(coffee.venue.id, 'tips');
    
    getJSON.response(photosUrl, venuePhotos);
    getJSON.response(tipsUrl, venueTips);

    function venuePhotos(response) {
        console.log(response);
        console.log(response.response.photos.items);

        var photoArray = response.response.photos.items;
        var photoObj;
        var photos = [];

        for (var i = 0; i < photoArray.length; i++) {
            photoObj = photoArray[i];
            photos[i] = photoUrl.get(photoObj, '300x300');

            console.log(photos[i]);
        }

        var photosNo = photos.length;
        console.log(photosNo);
        (photosNo > 10) ? photosNo = 10 : photosNo = photosNo;
        console.log(photosNo);

        var photosElement = document.querySelector('.photos');
        photosElement.innerHTML = '';
        console.log(photosElement);
        var photosHtml = ``;

        // for (let i = 0; i < photosNo; i++) {
        //     photosHtml +=
        //         `
        //     <div class="col-md-2">
        //         <a href="${photos[i]}"><img class="img-fluid" src="${photos[i]}" alt="photo-${coffee.venue.name}-${i}"></a>
        //     </div>
        //     `;
        // }

        photosHtml = '<div class="row">' + photosHtml + '</div>';
        photosElement.innerHTML = photosHtml;

        var slideShowElement = document.querySelector('.carousel-inner');
        var carouselIndicatorElement = document.querySelector('.carousel-indicators');
        slideShowElement.innerHTML = '';
        carouselIndicatorElement.innerHTML = '';
        console.log(slideShowElement);

        // Photos slide show
        for (let i = 0; i < photosNo; i++) {
            if (i == 0) {
                slideShowElement.innerHTML +=
                    `
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="${photos[i]}" alt="photo-${coffee.venue.name}-${i}">
                    </div>
                    `;
                carouselIndicatorElement.innerHTML +=
                    `
                    <li data-target="#carousel" data-slide-to="0" class="active"></li>
                    `;
            } else {
                slideShowElement.innerHTML +=
                    `
                    <div class="carousel-item">
                        <img class="d-block w-100" src="${photos[i]}" alt="photo-${coffee.venue.name}-${i}">
                    </div>
                    `;
                carouselIndicatorElement.innerHTML +=
                    `
                    <li data-target="#carousel" data-slide-to="${i}"></li>
                    `;
            }
        }

    }
    
    function venueTips(response) {

        var tipsArray = response.response.tips.items;
        var tipObj='';
        var tipsElement = document.querySelector('.tips');
        tipsElement.innerHTML = '';

        console.log('tips Array: ');
        console.log(tipsArray);

        // Check if there are ane tips
        if(!tipsArray.length) {
            tipsElement.innerHTML =
                `
                <div class="row justify-content-center">
                            <div class="col-md-6 col-sm-8 col-10">
                                <div>Sorry, no tips found!</div>
                            </div>
                        </div> 
                `;
        } else {
            for (var i = 0; i < tipsArray.length; i++) {
                // Check if tip contains coffee subject
                if(tipsArray[i].text.indexOf('coffee') < 0) {
                    tipsElement.innerHTML =
                        `
                        <div class="row justify-content-center">
                            <div class="col-md-6 col-sm-8 col-10">
                                <div>Sorry, no coffee related tips found!</div>
                            </div>
                        </div>
                        `;
                } else {
                tipsElement.innerHTML +=
                    `
                    <div class="row">
                        <div class="col-md-6 col-sm-8 col-10">
                            <div>${tipsArray[i].text}</div>
                            <div>user: ${tipsArray[i].user.firstName} ${tipsArray[i].user.lastName}</div>
                        </div>
                    </div>
                    `;
                }
            }
        }
    }

})(queryObject);

