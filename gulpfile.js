var gulp            = require('gulp');
// USEREF Plugin - Plugin sends HTML file to desred location and concatenates CSS and JS files
var useref          = require('gulp-useref');
var htmlmin         = require('gulp-htmlmin');
var uglify          = require('gulp-uglify');
var concat          = require('gulp-concat');
var cleanCss        = require('gulp-clean-css');
var autoprefixer    = require('gulp-autoprefixer');
var plumber         = require('gulp-plumber');
var sourcemaps      = require('gulp-sourcemaps');
var sass            = require('gulp-sass');
var babel           = require('gulp-babel');
var del             = require('del');
var zip             = require('gulp-zip');
var runSequence     = require('run-sequence');
var browserSync     = require('browser-sync').create();
var reload          = browserSync.reload;

// HANDLEBARS Plugins
var handlebars      = require('gulp-handlebars');
var handlebarsLib   = require('handlebars');
var declare         = require('gulp-declare');
var wrap            = require('gulp-wrap');

// Image compression
var imagemin        = require('gulp-imagemin');
var imageminPng     = require('imagemin-pngquant');
var imageminJpg     = require('imagemin-jpeg-recompress');

// File paths
var paths = {
    htmlPath:       'app/*.html',
    htmlDistTemp:   'dist/temp',
    htmlDist:       'dist',
    cssDist:        'app/css',
    jsPath:         'app/my-js/**/*.js',
    jsDist:         'app/js',
    imageDist:      'dist/images',
    dist:           'dist'
};

var DIST_PATH       = 'dist';
var SCRIPTS_PATH    = 'app/my-js/**/*.js';
var CSS_PATH        = 'app/css/**/*.css';
var TEMPLATES_PATH  = 'templates/**/*.hbs';
var IMAGES_PATH     = 'app/images/**/*.{png,jpeg,jpg,svg,gif}';
var EXPORT_FILE     = 'website.zip';


//
// GULP Functions
// ================================================

// Styles - CSS
//gulp.task('styles', function() {
//    console.log('Starting Styles Task');
//    return gulp.src(['app/css/reset.css', CSS_PATH])
//            .pipe(plumber(function(err) {
//                console.log('Styles error');
//                console.log(err);
//                this.emit('end');
//            }))
//            .pipe(sourcemaps.init())    // Sourcemaps initialization
//                .pipe(autoprefixer({
//                    browsers: ['last 2 versions', 'ie 8']
//                }))
//                .pipe(concat('styles.css'))
//                .pipe(cleanCss({ compatibility: 'ie8' }))
//            .pipe(sourcemaps.write())   // Sourcemaps write
//            .pipe(gulp.dest(DIST_PATH));
//});

// USEREF
//====================================================
gulp.task('useref', function() {
   return gulp.src(paths.htmlPath)
       .pipe(useref())
       .pipe(gulp.dest(paths.htmlDist));
});

// HTML - Minify
//====================================================
gulp.task('htmlminify', function() {
    return gulp.src(paths.htmlDist + '/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest(paths.htmlDist));
});

// Styles - SASS
// My Styles, Font Awesome and Bootstrap
//====================================================
gulp.task('styles', ['my-styles', 'stylesBs', 'stylesFa'], function() {
   console.log('Styles Task Started');
});

gulp.task('my-styles', function() {
    console.log('Starting Styles Task');
    return gulp.src('app/scss/styles.scss')
            .pipe(plumber(function(err) {
                console.log('Styles error');
                console.log(err);
                this.emit('end');
            }))
            .pipe(sourcemaps.init())
                .pipe(sass({
                    outputStyle: 'compressed'
                }))
                .pipe(autoprefixer({
                    browsers: ['last 3 major versions', 'ie 8'],
                    cascade: false
                }))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(paths.cssDist))
            .pipe(reload({stream: true}));
});

// Styles - Bootstrap SASS
//====================================================
gulp.task('stylesBs', function() {
    console.log('Starting Styles Task');
    // return gulp.src('node_modules/bootstrap/scss/bootstrap.scss')
    return gulp.src('app/scss/bootstrap.scss')
        .pipe(sourcemaps.init())    // Sourcemaps initialization
            .pipe(sass({
                outputStyle: 'compressed'
            }))
        .pipe(sourcemaps.write())   // Sourcemaps write
        .pipe(gulp.dest(paths.cssDist))
        .pipe(reload({stream: true}));
});

// Styles - Font Awesome
//====================================================
gulp.task('stylesFa', function() {
    console.log('Starting Font Awesome Styles Task');

    return gulp.src('app/scss/font-awesome/font-awesome.scss')
        .pipe(sourcemaps.init())    // Sourcemaps initialization
            .pipe(sass({
                outputStyle: 'compressed'
            }))
        .pipe(sourcemaps.write())   // Sourcemaps write
        .pipe(gulp.dest(paths.cssDist));
});

// SERVE Task
//====================================================
gulp.task('serve', ['styles'], function() {
    console.log('Serve Task Started');

    browserSync.init({
        server: "./app"
    });

    gulp.watch(SCRIPTS_PATH, ['scripts']);
    gulp.watch('app/scss/**/*.scss', ['styles']);
    gulp.watch(paths.htmlPath).on('change', reload);
});

// Scripts
//====================================================
gulp.task('scripts', function() {
    console.log('Starting Scripts Gulp Task');

    return gulp.src(paths.jsPath)
        .pipe(plumber(function(err) {
            console.log('Scripts Task Error');
            console.log(err);
            this.emit('end');
        }))
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.jsDist))
        .pipe(reload({stream: true}));
});

gulp.task('scriptsBs', function() {
    
    return gulp.src('node_modules/bootstrap/dist/js/bootstrap.js')
        .pipe(sourcemaps.init())
            .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.jsDist));
});

// Images
//====================================================
gulp.task('images', function() {
    return gulp.src(IMAGES_PATH)
            .pipe(imagemin([
                imagemin.gifsicle(),
                imagemin.optipng(),
                imagemin.jpegtran(),
                imagemin.svgo(),
                imageminJpg(),
                imageminPng()
            ]))
            .pipe(gulp.dest(paths.imageDist));
});

// Templates
//====================================================
gulp.task('templates', function() {
   console.log('Starting Templates Gulp Task');
   
   return gulp.src(TEMPLATES_PATH)
           .pipe(handlebars({
               handlebars: handlebarsLib
           }))
            .pipe(wrap('Handlebars.template(<%= contents %>)'))
            .pipe(declare({
                namespace: 'templates',
                noRedeclare: true
            }))
            .pipe(concat('templates.my-js'))
            .pipe(gulp.dest(DIST_PATH));
});

// CLEAN Dist Folder
//====================================================
gulp.task('clean', function() {
    return del.sync([
        paths.dist,
        // paths.cssDist + '/styles.css',
        // paths.jsDist + '/scripts.js',
        EXPORT_FILE
    ]);
});

// EXPORT Dist Folder
//====================================================
gulp.task('export', function() {
    return gulp.src('app/**/*')
        .pipe(zip(EXPORT_FILE))
        .pipe(gulp.dest('./'));
});

// Default Task
//====================================================
gulp.task('default', ['clean', 'images', 'templates', 'styles', 'scripts'], function () {
    console.log('Starting Default Gulp Task');
});

// Watch
//====================================================
gulp.task('watch', ['default'], function() {
    console.log('Starting Watch Task');

    gulp.watch(SCRIPTS_PATH, ['my-js']);
//    gulp.watch(CSS_PATH, ['styles']);
    gulp.watch('app/scss/**/*.scss', ['styles']);
    gulp.watch(TEMPLATES_PATH, ['templates']);
});

// PRODUCTION Task
//====================================================
gulp.task('build', function(callback) {
    console.log('Making Production Files Task Started');

    runSequence(
        'clean',
        ['styles', 'scripts', 'images'],
        'useref',
        'htmlminify',
        callback
    );
});